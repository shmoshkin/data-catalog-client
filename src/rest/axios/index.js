const axios = require('axios');
var tokenService = require ('../../utils/token');


export const authInstance = axios.create({
    baseURL: `${process.env.REACT_APP_SERVER}/auth`,
    withCredentials: true,
    credentials: 'same-origin',
    timeout: 15000,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },  
});

authInstance.interceptors.request.use(function (config) {    
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

// Add a response interceptor
authInstance.interceptors.response.use(function (response) {
  return response.status === 401 ? tokenService.clearToken() : response  
  }, function (error) {
    return error.response.status === 401 ? tokenService.clearToken() : Promise.reject(error)
  });


  export const axiosInstance = axios.create({
    baseURL: `http://localhost:8080`,
    withCredentials: true,
    credentials: 'same-origin',
    timeout: 15000,
    headers: {
      Accept: 'multipart/form-data',
      'Access-Control-Allow-Origin':  '*',
      'Access-Control-Allow-Methods': 'POST,OPTIONS',
      'Content-Type': 'multipart/form-data'
    },  
});

axiosInstance.interceptors.request.use(function (config) {    
  return config;
}, function (error) {
  return Promise.reject(error);
});