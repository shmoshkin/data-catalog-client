
import Cookies from 'js-cookie';
import {authInstance} from '../../rest/axios';

export const getToken = () => Cookies.get("access-token")
export const clearToken = () => Cookies.remove("access-token")
export const getRefreshToken = () => Cookies.get("refresh-token")
export const clearRefreshToken = () => Cookies.remove("refresh-token")

export const getSplitToken = () => getToken()? getToken().split(".") : null
export const getTokenPayload = () => getSplitToken() ? getSplitToken()[1] : null
export const getTranslatedTokenString = () => getTokenPayload() ? atob(getTokenPayload()) : null
export const getTranslatedTokenAsJson = () => getTranslatedTokenString() ? JSON.parse(getTranslatedTokenString()) : null
export const getName = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().name : null
export const getEmail = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().email : null
export const getAdmin = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().admin : null
export const getRole = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().role : null
export const getExp = () => getTranslatedTokenAsJson() ? getTranslatedTokenAsJson().exp : null

export const validToken = () => ((new Date().getTime() / 1000) < (getExp() - 30))
export const refreshToken = () => authInstance.get('/refreshToken');