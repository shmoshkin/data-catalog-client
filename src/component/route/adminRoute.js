import React from "react";
import { Redirect, Route } from "react-router-dom";
import {getAdmin} from '../../utils/token'
import {HOME} from '../../constants/route'

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props => 
            getAdmin() ? (
            <Component {...props}/>
            ) : (
                <Redirect
                    to={{
                        pathname: HOME,
                        state: {from: props.location}
                    }}
                />                
            )
        }
    />  
)

export default PrivateRoute;