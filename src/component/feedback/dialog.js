import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';


const FormDialog= (props) => {

  const [newName, setnewName] = React.useState("");

  const handleClose = (e) => {
    props.setOpen(false);
  };

  const handleUpdate = (e) =>{
    props.onUpdate(props.file.name, newName)
    props.setOpen(false);
    setnewName("")
  }

  return (
    <div>
      <Dialog open={props.opened} onClose={handleClose} aria-labelledby="form-dialog-title">
  <DialogTitle id="form-dialog-title">rename file</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="new name"
            type="email"
            fullWidth
            onChange = {(e) =>setnewName(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleUpdate} color="primary">
            rename
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default FormDialog