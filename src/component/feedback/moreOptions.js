import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import FormDialog from './dialog';
import EditIcon from '@material-ui/icons/Edit';


const Options = (props) => {
  const ITEM_HEIGHT = 48;
  const [opened, setOpenDialog] = React.useState(false);
  
  const options = [
    {name:'delete', func: () => handleDelete(), icon: <DeleteIcon style ={{marginLeft: '80px'}}/>},
    {name:'rename', func: () => handleRename(), icon:  <EditIcon style ={{marginLeft: '50%'}}/>},
  // {name:'properties', func: () => handleProperties(), icon:  <MoreVertIcon  style ={{marginLeft: '37%'}}/>},
  ]

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };
    
    const handleDelete = () => {
      props.delete(props.file.name)
      setAnchorEl(null);
    }

    const handleRename = () => {
      setOpenDialog(true)
      setAnchorEl(null);
    }

    // const handleProperties = () => {
    //   setAnchorEl(null);
    // }
  return (
    <div>
        <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            ITEM_HEIGHT: ITEM_HEIGHT * 4.5,
            width: '20ch',
          },
        }}
      >
        {options.map((option) => (
          <MenuItem key={option.name} onClick={option.func}>
            {option.name}
           {option.icon}
          </MenuItem>
        ))}
      </Menu>
      <FormDialog opened={opened} setOpen={setOpenDialog} onUpdate={props.onUpdate} file={props.file}></FormDialog>
    </div>
  );
}

export default Options;