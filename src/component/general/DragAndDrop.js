import React,  {useMemo} from 'react';
import { useDropzone } from 'react-dropzone'
import { makeStyles } from '@material-ui/core/styles';
import AddIcon  from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';  

const useStyles = makeStyles((theme) => ({
  datasetcontainer: {
      height: "93vh",
  },
}));


const baseStyle = {
  flex: 2,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out',
};

const activeStyle = {
  borderColor: '#2196f3'
};

const acceptStyle = {
  borderColor: '#00e676'
};

const rejectStyle = {
  borderColor: '#ff1744'
};


function DragAndDrop(props) {
  const classes = useStyles();

  const {getRootProps, getInputProps, open,isDragActive,
    isDragAccept,
    isDragReject} = useDropzone({
    noClick: true,
    noKeyboard: true,
      onDrop: acceptedFiles => props.onDrop(acceptedFiles)
  });

  const style = useMemo(() => ({
    ...baseStyle,
    ...(isDragActive ? activeStyle : {}),
    ...(isDragAccept ? acceptStyle : {}),
    ...(isDragReject ? rejectStyle : {})
  }), [
    isDragActive,
    isDragReject
  ]);

  return (
      <div {...getRootProps({style})} className={classes.datasetcontainer}> 
        <input {...getInputProps()} />
        {props.children[0]}
        <h4>Files</h4>
        <div style={{ height: "80vh", width:'100%', overflowY: 'scroll'}}>
        {props.children[1]}
        </div>
        <Fab color="secondary" className={classes.fab} onClick={open}>
          <AddIcon />
        </Fab>
      </div>

  );
}

export default DragAndDrop;
