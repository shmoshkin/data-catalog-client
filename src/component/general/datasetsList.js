
import React from 'react';
import Dataset from './dataset'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
       flexWrap: 'wrap',
    },
  }));

const DatasetsList = (props) => {

    const classes = useStyles();

    const datasets = props.files.map((file) => {
        return <Dataset key={file.name} file={file} delete={props.delete} onUpdate={props.onUpdate}/>;
      });
  
    return (
        <div className={classes.root}>
         {datasets}  
        </div>
    );
  }
 
  export default DatasetsList;