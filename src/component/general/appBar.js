import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import {map} from 'ramda';
import {Link} from 'react-router-dom'
import UserCard from '../Login/UserCard'
import {getAdmin} from '../../utils/token'

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  inputRoot: {
    color: 'inherit',
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },    
  },
  link: {
    cursor: 'pointer',
    color: "white",
    textDecoration: "blink"
  }    
}));

const AppBarComponent = (props) => {

  const classes = useStyles();

  const {tabs} = props;

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          <IconButton color="inherit">
            <Typography className={classes.title} variant="h6" noWrap>
                Data Catalog
            </Typography>     
          </IconButton>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
              {
                map((tab) => {
                  const isVisible = tab.admin ? getAdmin() : true
                  return (isVisible ? <div key={tab.title}>
                    <Link  to={tab.link} className={classes.link}>
                    <IconButton color="inherit">
                      <Typography>{tab.title}</Typography>
                    </IconButton>
                    </Link>
                    </div> : null)
                }, tabs)
              }
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"              
              color="inherit"
            >
              <AccountCircle />
            </IconButton>           
          </div>     
        </Toolbar>
      </AppBar>        
    </div>
  );
}

export default AppBarComponent;