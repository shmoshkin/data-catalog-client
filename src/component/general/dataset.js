
  import React  from 'react';
  import Card from '@material-ui/core/Card';
  import { makeStyles } from '@material-ui/core/styles';
  import CardMedia from '@material-ui/core/CardMedia'
  import CardContent from '@material-ui/core/CardContent';
  import Typography from '@material-ui/core/Typography';
  import IconButton from '@material-ui/core/IconButton';
import Options from '../feedback/moreOptions'

  const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: '68%', // 16:9,
      marginTop:'30'
    },
  }));


function Dataset(props) {

  const downloadUrl = "amitnie/uploaded/amitniemann@gmail.com"
  const fileDownloadUrl = `${downloadUrl}/${props.file.name}`

  const fileType =() =>{
    if(props.file.type == 'text/plain') return "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT7j32YtwYyra2u8TLoVB9NAMdHX5j43tzoD9OZgTVIvNlcua5-&usqp=CAU"
    else if (props.file.type == 'png')return "https://cdn.windowsreport.com/wp-content/uploads/2018/04/png-file-format-symbol_318-45313.jpg"
    else if (props.file.type == 'json') return "https://cdn.windowsreport.com/wp-content/uploads/2018/04/png-file-format-symbol_318-45313.jpg"
    else if (props.file.type == 'application/vnd.ms-excel') return "https://getdrawings.com/free-icon/icon-extension-55.png"
    else return "https://lh3.googleusercontent.com/proxy/kQy-XDSYWQRrf8G0s-UK2aVew-8R6OHnlP_SkMM9Uarjfj1fdm2qGcBBgmLQaQr493qUSF8EnLhEOekYA8MHgShF8bd_8g8Gp2oNa936pdBuYvlicZht"
  }

    const classes = useStyles();
    return (
    
        <Card style={{ width: '11rem', marginLeft: '20px', marginTop: '20px', cursor:'pointer'}} > 
        <Options file={props.file} delete={props.delete} onUpdate={props.onUpdate}></Options>
        <a href={fileDownloadUrl} download>
          <div>
        <CardMedia
       className={classes.media}
       image={fileType()}
     />
     </div>
        <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {props.file.name}
        </Typography>
      </CardContent>
      </a>
        </Card>
       
    );
}


export default Dataset;
