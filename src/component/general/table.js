import React, {Component} from 'react';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { createMuiTheme } from '@material-ui/core/styles';
import MaterialTable from 'material-table';

class Table extends Component {

    constructor(props) {
      super(props);
      this.theme = createMuiTheme({
        palette: {
          primary: {
            main: '#1565c0',
          },
          secondary: {
            main: '#1565c0',
          },
        },
      });
    }

    render() {

      const {columns, data, title, onRowDelete, onRowUpdate} = this.props;
      return (
        <MuiThemeProvider theme={this.theme}>
          <MaterialTable
            title={title}
            columns={columns}
            data={data}
            // options={{
            //   selection: true
            // }}
            editable={{                             
            onRowUpdate: (newData, oldData) =>
                new Promise((resolve, reject) => {
                    setTimeout(() => {
                        onRowUpdate(newData, oldData)
                        resolve();
                    }, 1000);
                }),
            onRowDelete: oldData =>
                new Promise((resolve, reject) => {
                    setTimeout(() => {
                        onRowDelete(oldData)
                        resolve();
                    }, 1000);
                })
            }}
          />
        </MuiThemeProvider>
      )
    }
  }

export default Table;