import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '120%',
    },
  },
}));

const Search = (props) => {
  const classes = useStyles();
  const handlehange = (e) =>{
    props.setsearchValue(e.target.value);
    props.setfilteredFiles(props.files.filter( fileName => fileName.name.includes(e.target.value)))
  }

  return (<span>
    <TextField
    className={classes.root}
              autoFocus
              margin="dense"
              id="name"
              onChange={(event) => handlehange(event)}
              label="search"
              type="text"
            />
  </span>
  );
}

export default Search