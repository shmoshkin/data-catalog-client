import React from 'react'
import { withStyles } from '@material-ui/styles';
import Paper from '@material-ui/core/Paper';
import { useSubscription, useMutation, useQuery } from '@apollo/react-hooks';

import Loading from '../../component/feedback/loading'
import Table from '../../component/general/table'
import {SUBSCRIPTION_GET_ALL_USERS,
        DELETE_USER_BY_EMAIL,
        UPDATE_USER_BY_EMAIL} from '../../graphql/queries/users'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: 1,
      width: "100%",
      height: "100%"
    }    
  },
  loading: {
    position: 'absolute',
    left: '50%',
    top: '50%'
  }
});

const ManageUsersContainer = ({classes}) => {

  const onRowDelete = (oldData) => {
    deleteUser({ variables: { email: oldData.email } })
  }
  
  const onRowUpdate = (newData, oldData) => {
    updateUser({ variables: { email: oldData.email, role: newData.role, admin: newData.admin } })
  }

  const [updateUser, updateRes] = useMutation(UPDATE_USER_BY_EMAIL);
  const [deleteUser, deleteRes] = useMutation(DELETE_USER_BY_EMAIL);

  const {
    data,
    loading,
    error
  } = useSubscription(SUBSCRIPTION_GET_ALL_USERS);

  if (loading) return (<div className={classes.loading}><Loading/></div>)
  if (error) return <p>error</p>
  
  return (
    <div className={classes.root}>
      <Paper elevation={3}>
        <Table
          columns={[
            {
              title: 'Name', field: 'name',
              render: rowData => `${rowData.firstName} ${rowData.lastName}`,
              editable: 'never'
            },
            { title: 'Email', field: 'email', editable: 'never' },
            { title: 'Role', field: 'role'},
            { title: 'Admin', field: 'admin', type: 'boolean'},
            { title: 'Create Date', field: 'created_at', type:'date', editable: 'never'},
          ]}
          data={data.users}
          title="Users"
          onRowUpdate={onRowUpdate}
          onRowDelete={onRowDelete}
        />
      </Paper>
    </div>
  )
}

export default withStyles(styles)(ManageUsersContainer)