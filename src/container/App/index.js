import React, {useEffect} from 'react';
import { Route, Switch, withRouter} from 'react-router-dom'

import LoginContainer from '../LoginContainer';
import ManageUsersContainer from '../ManagerContainer';
import HomeContainer from '../HomeContainer';
import AddDataSetContainer from '../AddDataSetContainer';
import AppBarContainer from '../AppBarContainer';
import NotFound from '../../component/general/notFound';
import PrivateRoute from '../../component/route/privateRoute';
import AdminRoute from '../../component/route/adminRoute';
import {HOME, LOGIN, APP_BAR, MANAGER, ADD_DATA_SET} from '../../constants/route'


import './index.css';


const App = (props) => {

    return(    
        <>
          <PrivateRoute path={APP_BAR} component={AppBarContainer} />          
          <Switch>
            <Route path={LOGIN} component={LoginContainer} />            
            <AdminRoute path={MANAGER} component={ManageUsersContainer}/>
            <PrivateRoute path={HOME} component={HomeContainer} />
            <PrivateRoute path={ADD_DATA_SET} component={AddDataSetContainer} />
            <PrivateRoute component={NotFound} />
          </Switch>
        </>)   
};

export default withRouter(App);