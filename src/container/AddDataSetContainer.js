import React,  { useState } from 'react';
import DatasetsList from '../component/general/datasetsList'
import DragAndDrop from '../component/general/DragAndDrop'
import {getEmail} from '../utils/token'
import { useSubscription, useMutation } from '@apollo/react-hooks';
import Loading from '../component/feedback/loading'
import { makeStyles } from '@material-ui/core/styles';
import {ADD_DATASET,
  SUBSCRIPTION_GET_ALL_DATASETS,
  DELETE_DATASET,
  UPDATE_DATASET} from '../graphql/queries/datasets'
import Paper from '@material-ui/core/Paper';
import {axiosInstance} from '../rest/axios'
import to from 'await-to-js';
import Search from '../component/general/search'
import DynamicSnackbar from '../component/feedback/snackbar';
import FormDialog from '../component/feedback/dialog';



const AddDataSetContainer = () => {

  const [filteredFiles, setfilteredFiles] = useState([]);
  const [searchValue, setsearchValue] = useState('');
  const [snackbar, setSnackbar] = useState({
    open: false,
    message: ""
  });

  const setSnackbarOpen = (value) => setSnackbar({open: value})

  const deleteFile = (name) => {
    deleteDataset(({variables: { user_email: getEmail(), name: name}}))
  }

  const onUpdate = (name, newName) => {
    updateDataset(({ variables: { user_email: getEmail(), name: name, newName: newName } }))
  }

  const [addDataSet, addRes] = useMutation(ADD_DATASET);
  const [deleteDataset, deleteRes] = useMutation(DELETE_DATASET);
  const [updateDataset, updateRes] = useMutation(UPDATE_DATASET);
  

  const {
    data,
    loading,
    error
  } = useSubscription(SUBSCRIPTION_GET_ALL_DATASETS,  { variables: { user_email: getEmail()}, onSubscriptionData: ({subscriptionData }) => {
    setfilteredFiles(subscriptionData.data.data_sets_files.filter( fileName => fileName.name.includes(searchValue)))
  }},
  );


  if (loading) return (<div ><Loading/></div>)
  if (error) return <p>error</p> 

  const alertExistedFile = (acceptedfile) => {
    if (data.data_sets_files.map(f => f.name).includes(acceptedfile.name)) setSnackbar({open: true ,message: "file is existing it will be overwrite"})
  }

  const onDrop = async (acceptedFiles) => {

      acceptedFiles.map(async(acceptedfile) =>{
        alertExistedFile(acceptedfile)
        console.log(acceptedfile)
        var formData = new FormData();
        formData.append("uploadfile", acceptedfile);
        const url = `upload/csv`;
        const [err,res] = await to(axiosInstance.post(url, formData))
        if(err) 
          {
            setSnackbar({open: true ,message: err.message})
          }
          else{
          addDataSet({variables: { user_email: getEmail(), type: acceptedfile.type, size: acceptedfile.size, name: acceptedfile.name}})
          setSnackbar({open: true ,message: "dataset was added succssfully"})
        }
      }
    )
  }




  return (
    <div>
    <Paper elevation={3}> 
      <DragAndDrop onDrop={onDrop}>
      <Search setfilteredFiles={setfilteredFiles} setsearchValue={setsearchValue} files={data.data_sets_files} ></Search>
      <DatasetsList files={filteredFiles}  delete={deleteFile} onUpdate={onUpdate}/>  
      </DragAndDrop>
      </Paper>
      <DynamicSnackbar
          open={snackbar.open}
          setOpen={setSnackbarOpen}
          message={snackbar.message}
        />
        <FormDialog/>
     </div>
  );
  
}

export default AddDataSetContainer