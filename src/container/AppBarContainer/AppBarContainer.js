import React, {Component} from 'react';
import {HOME, MANAGER, ABOUT, ADD_DATA_SET} from '../../constants/route'
import AppBarComponent from '../../component/general/appBar';


class AppBarContainer extends Component {

    state = {
        tabs:[
            {title: "Home", link: HOME, admin: false},
            {title: "Manager", link: MANAGER, admin: true},
            {title: "About", link: ABOUT, admin: false},
            {title: "Add dataset", link: ADD_DATA_SET, admin: false}
        ],
        userCardOpen: false
    }

    onClickUserCard = () => {
        this.setState({userCardOpen: !this.state.userCardOpen})
    }

    render() {
        return (
            <AppBarComponent
                tabs={this.state.tabs}
                userCardOpen={this.state.userCardOpen}
                onClickUserCard={this.onClickUserCard}
            />
        )
    }
}

export default AppBarContainer