import { gql } from 'apollo-boost';
export const ADD_DATASET = gql`mutation MyMutation($user_email: String!, $type: String!, $size: float8!,$name: String!){
    insert_data_sets_files(objects: {user_email: $user_email,type: $type, size: $size, name: $name}, on_conflict: {where: {name: {_eq: $name}, user_email: {_eq: $user_email}}, update_columns: name, constraint: data_sets_files_pkey}) {
      affected_rows
    }
  }`
  
  
  
  export const SUBSCRIPTION_GET_ALL_DATASETS = gql `subscription MySubscription($user_email: String!) {
    data_sets_files(where: {user_email: {_eq: $user_email}}) {
      name
      date
      size
      type
      user_email
    }
  }
  `
  
  
  
  export const DELETE_DATASET= gql `mutation delete_dataset($user_email: String!, $name:String!) {
    delete_data_sets_files(where: {user_email: {_eq: $user_email},name:  {_eq: $name}}) {
      affected_rows
    }
  }`


  
export const UPDATE_DATASET = gql`mutation MyMutation($user_email: String!, $name:String!, $newName:String!) {
  update_data_sets_files(where: {user_email: {_eq: $user_email}, name: {_eq: $name}}, _set: {name: $newName}) {
    affected_rows
  }
}`
