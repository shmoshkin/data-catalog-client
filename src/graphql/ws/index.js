import { WebSocketLink } from 'apollo-link-ws';
import { SubscriptionClient } from "subscriptions-transport-ws";
import MessageTypes from 'subscriptions-transport-ws/dist/message-types'

import {getToken, getRole} from '../../utils/token';

var wsLinkInstance;
var wsClientInstance;

export const getWsLinkInstance = () => {

  if(!wsLinkInstance){
    wsLinkInstance = createWsLinkInstance();
  }

  return wsLinkInstance
}

const getWsClientInstance = () => {

  if(!wsClientInstance){
    wsClientInstance = createWsClientInstance();
  }

  return wsClientInstance
}

const createWsClientInstance = () => 
  new SubscriptionClient(process.env.REACT_APP_HASURA_WEB_SOCKET_ENDPOINT, {
    reconnect: true,
    timeout: 30000,    
    lazy: true,
    connectionParams: () => {
      return ({headers: {
        Authorization: getToken() ? `Bearer ${getToken()}` : "",
        "x-hasura-role": getRole() ? getRole() : 'anonymus'
      }})
    }
  })

export const connectWebsocket = () => {
  getWsLinkInstance().subscriptionClient.connect();
};  

const createWsLinkInstance = () => {
 
  if(!wsClientInstance){
    wsClientInstance = getWsClientInstance();
  }

  wsLinkInstance = new WebSocketLink(wsClientInstance)
  return wsLinkInstance;
}

export const closeWs = () => {
  getWsClientInstance().close(true);
}
export const restartWebsockets = () => {
  // Copy current operations
  const operations = Object.assign({}, wsLinkInstance.operations)

  // Close connection
  wsLinkInstance.close(true)

  // Open a new one
  wsLinkInstance.connect()

  // Push all current operations to the new connection
  Object.keys(operations).forEach(id => {
    wsLinkInstance.sendMessage(
      id,
      MessageTypes.GQL_START,
      operations[id].options
    )
  })
}