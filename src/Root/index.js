import React, {useEffect} from 'react';

import { withRouter, BrowserRouter as Router } from 'react-router-dom'
import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from "apollo-client";

import { split, ApolloLink } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { onError } from 'apollo-link-error';
import { RetryLink } from "apollo-link-retry";
import { setContext } from 'apollo-link-context';
import to from 'await-to-js'

import App from '../container/App';
import {getWsLinkInstance, closeWs} from '../graphql/ws'
import {getHttpLink} from '../graphql/http';
import {getToken, getRole, validToken, refreshToken, clearToken} from '../utils/token';
import {useInterval} from '../hooks/useInterval'
import {HOME, LOGIN} from '../constants/route'

var isRefreshing = false;

const Root = (props) => {
  
  const authLink = setContext((_, { headers }) => {
      return {
        headers: {
          ...headers,
          Authorization: getToken() ? `Bearer ${getToken()}` : "",
          "x-hasura-role": getRole() ? getRole() : 'anonymus'
        }
      }
  });
  
  const wsLink = getWsLinkInstance();
  const httpLink = getHttpLink();
  const authHttpLink = authLink.concat(httpLink)
  
  const link = split(
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query);
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink,
    authHttpLink
  );
  
  const client = new ApolloClient({
    link: ApolloLink.from([     
    new RetryLink(),
    link
    ]),
    cache: new InMemoryCache()
  });

  useEffect(() => {
    if (getToken() && validToken()) {
      if(props.location.pathname === LOGIN ||
        props.location.pathname === '/') props.history.push(HOME);

    } else if(props.location.pathname !== LOGIN) {
      clearToken();
    }
  }, []);

  useInterval(async () => {
    if((!getToken() || !validToken()) && (props.location.pathname !== LOGIN) && (!isRefreshing)) {
      isRefreshing = true
      const [err,  newToken] = await to(refreshToken())
      closeWs()
      isRefreshing = false;
    }
  }, 15000)

  return (
    <ApolloProvider client={client}>
        <App client={client}/>
    </ApolloProvider>
  )
};

export default withRouter(Root);